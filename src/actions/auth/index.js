import axios from 'axios';
import {REGISTER_CONST, LOGIN_CONST, API_URL} from '../../consts';

export const postAuthLogin = function (authData) {
    const request = function () {
        return {type: LOGIN_CONST.LOGIN_CONST_REQUEST};
    };

    const success = function (response) {
        localStorage.setItem('user', JSON.stringify(response.data.user));
        localStorage.setItem('token', JSON.stringify(response.data.token));
        return {type: LOGIN_CONST.LOGIN_CONST_SUCCESS, loading: false, token: response.data.token};
    };

    const fail = function (errorMessage) {
        return {type: LOGIN_CONST.LOGIN_CONST_FAIL, errorMessage: errorMessage};
    };

    return dispatch => {
        dispatch(request());

        axios.post(API_URL.LOGIN, authData).then(response => {
            if (response.data.code === 200) {
                dispatch(success(response));
            } else {
                dispatch(fail(response.data.message));
            }
        }).catch(error => {
            dispatch(fail(error.message));
        });
    };
};


export const postAuthRegister = function (data) {
    const request = function () {
        return {type: REGISTER_CONST.REGISTER_CONST_REQUEST};
    };

    const success = function (response) {
        localStorage.setItem('user', JSON.stringify(response.data.user));
        localStorage.setItem('token', JSON.stringify(response.data.token));
        return {type: REGISTER_CONST.REGISTER_CONST_SUCCESS, loading: false, token: response.data.token};
    };

    const fail = function (errorMessage) {
        return {type: REGISTER_CONST.REGISTER_CONST_FAIL, errorMessage: errorMessage};
    };

    return dispatch => {
        dispatch(request());

        axios.post(`${API_URL}/api/auth/login`, data).then(response => {
            if (response.data.code === 200) {
                dispatch(success(response));
            } else {
                dispatch(fail(response.data.message));
            }
        }).catch(error => {
            dispatch(fail(error.message));
        });
    };
}
