import {getGDVs, getProcessingTrades} from './home';
import {postAuthLogin, postAuthRegister} from './auth';
export {
  getGDVs,
  postAuthLogin,
  postAuthRegister,
  getProcessingTrades
};