import axios from 'axios';
import {GET_DGV, API_URL, GET_PROCESSING_TRADE} from '../../consts';
export function getGDVs() {
  const request = function () {
    return {type: GET_DGV.GET_GDV_REQUEST};
  };

  const success = function (response) {
      console.log('response', response);
      return {type: GET_DGV.GET_GDV_SUCCESS, loading: false, gdvs: response.data.gdvs};
  };

  const fail = function (error) {
      return {type: GET_DGV.GET_GDV_FAIL, error: error};
  };

  return dispatch => {
    dispatch(request());
      axios.get(`${API_URL}api/get_gdv`).then(response => {
        switch(response.data.code) {
          case 200:
            dispatch(success(response));
            
          default: 
            dispatch(fail(response.data.message));
        }
      }).catch(error => {
          dispatch(fail(error.message));
      });
  }
}

export const getProcessingTrades = function() {
  const request = function () {
    return {type: GET_PROCESSING_TRADE.GET_PROCESSING_TRADE_REQUEST};
  };

  const success = function (response) {
      console.log('response', response);
      return {type: GET_PROCESSING_TRADE.GET_PROCESSING_TRADE_SUCCESS, loading: false, trades: response.data.trades};
  };

  const fail = function (error) {
      return {type: GET_PROCESSING_TRADE.GET_PROCESSING_TRADE_FAIL, error: error};
  };

  return dispatch => {
    dispatch(request());
    axios.get(`${API_URL}api/get_processing_trades`).then(response => {
      switch(response.data.code) {
        case 200:
          dispatch(success(response));
          
        default: 
          dispatch(fail(response.data.message));
      }
    }).catch(error => {
        dispatch(fail(error.message));
    });
  }
}