import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Home from './components/home';
import Social from './components/social';

import Message from './components/message';
import Rank from './components/rank';
import Guild from './components/guild';

import Login from './components/auth/login';
import Register from './components/auth/register';

function RouteApp() {
  return (
    <Switch>
      <Route exact path = '/' component = {Home} />
      <Route exact path = '/login' component = {Login} />
      <Route exact path = '/register' component = {Register} />
      <Route exact path = '/social' component = {Social} />
      <Route exact path = '/message' component = {Message} />
      <Route exact path = '/rank' component = {Rank} />
      <Route exact path = '/guild' component = {Guild} />
    </Switch>
  );
}

export default RouteApp;
