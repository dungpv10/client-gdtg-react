import React from 'react';
import ReactDOM from 'react-dom';
import thunk from "redux-thunk";
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import {AppContainer} from 'react-hot-loader';
import {BrowserRouter} from 'react-router-dom';
import RoutApp from './route';
import * as serviceWorker from './serviceWorker';
import reducers from './reducers';
const initialState = window.__INITIAL_STATE__;

const store = createStore(reducers, applyMiddleware(thunk), initialState);

const render = (Component) => {
  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <BrowserRouter>
          <Component/>
        </BrowserRouter>
      </Provider>
    </AppContainer>, document.getElementById('root')
  );
}
render(RoutApp);

if (module.hot) {
  module.hot.accept('./route', () => {
    const nextApp = require('./route').default;
    render(nextApp);
  });
}

serviceWorker.unregister();
