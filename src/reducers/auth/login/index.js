import {LOGIN_CONST} from '../../../consts/auth';

function postAuthLogin(state = {loading: true, errorMessage: null, token: null, user: null}, action) {
    switch (action.type) {
        case LOGIN_CONST.LOGIN_CONST_REQUEST:
            return {...state};

        case LOGIN_CONST.LOGIN_CONST_SUCCESS:
            return {...state, loading: false, token: action.token, user: action.user};

        case LOGIN_CONST.LOGIN_CONST_FAIL:
            return {...state, loading: false, errorMessage: action.errorMessage};

        default:
            return {...state};
    }
}

export default postAuthLogin;
