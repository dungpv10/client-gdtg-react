import {REGISTER_CONST} from '../../../consts/auth';

function postAuthRegister(state = {loading: true, errorMessage: null, token: null, user: null}, action) {
    switch (action.type) {
        case REGISTER_CONST.REGISTER_CONST_REQUEST:
            return {...state};

        case REGISTER_CONST.REGISTER_CONST_SUCCESS:
            return {...state, loading: false, token: action.token, user: action.user};

        case REGISTER_CONST.REGISTER_CONST_FAIL:
            return {...state, loading: false, errorMessage: action.errorMessage};

        default:
            return {...state};
    }
}

export default postAuthRegister;
