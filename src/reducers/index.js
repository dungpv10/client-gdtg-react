import {combineReducers} from 'redux';
import {getGDVs, getProcessingTrades} from './home';
import postAuthLogin from './auth/login';
import postAuthRegister from './auth/register';
const reducers = combineReducers({
  getGDVs,
  postAuthLogin,
  postAuthRegister,
  getProcessingTrades
});

export default reducers;