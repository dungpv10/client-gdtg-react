import {GET_DGV, GET_PROCESSING_TRADE} from '../../consts';
const getGDVs = function(state = {loading: true, error: null, gdvs: []}, action) {
    switch (action.type) {
        case GET_DGV.GET_GDV_REQUEST:
            return {...state};

        case GET_DGV.GET_GDV_SUCCESS:
            return {...state, loading: false, gdvs: action.gdvs};

        case GET_DGV.GET_GDV_FAIL:
            return {...state, loading: false};

        default:
            return {...state};
    }
}

const getProcessingTrades = function (state = {loading: true, error: null, trades: []}, action) {
    switch (action.type) {
        case GET_PROCESSING_TRADE.GET_PROCESSING_TRADE_REQUEST:
            return {...state};

        case GET_PROCESSING_TRADE.GET_PROCESSING_TRADE_SUCCESS:
            return {...state, loading: false, trades: action.trades};

        case GET_PROCESSING_TRADE.GET_PROCESSING_TRADE_FAIL:
            return {...state, loading: false};

        default:
            return {...state};
    }
}
export {getGDVs, getProcessingTrades};