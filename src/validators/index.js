import React from 'react';
import validator from 'validator';
import ErrorMessage from "./error_message";

const required = (value, props) => {
    if (!value.toString().trim().length) {
        const {label} = props;
        return (<ErrorMessage message={`${label} không được bỏ trống`}/>);
    }
};

const email = (value) => {
    if (!validator.isEmail(value)) {
        return (<ErrorMessage message={`Email sai định dạng`}/>);
    }
};

const validateUsername = (value) => {
    if(value.toLowercase().indexOf('gdv') !== -1) {
        return (<ErrorMessage message={`Tên đăng nhập không được bắt đầu bằng gdv`}/>);
    }
}

const lt = (value, props) => {
    if (value.toString().trim().length > props.maxLength) {
        return (<ErrorMessage message={`Không được nhập quá ${props.maxLength} ký tự`}/>);
    }
};

const gt = (value, props) => {
    if (value.toString().trim().length < props.minLength) {
        return (<ErrorMessage message={`Không được nhập ít hơn ${props.minLength} kí tự`}/>);
    }
};

const passwordConfirm = (value, props, components) => {
    if (value !== components['password'][0].value) {
        return (<ErrorMessage message={'Xác nhận mật khẩu không đúng'}/>);
    }
};

const validateImage = (imageList) => {
    if (imageList.length === 0) {
        return (<ErrorMessage message={'Vui lòng chọn file'}/>);
    }

};


export {
    email, required, lt, gt, passwordConfirm, validateImage, validateUsername
};
