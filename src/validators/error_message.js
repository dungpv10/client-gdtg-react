import React, {Component} from 'react';

class ErrorMessage extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (<div className="help-block has-error">{this.props.message}</div>);
    }

}

export default ErrorMessage;
