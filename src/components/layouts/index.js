import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import Header from './components/header';
import Footer from './components/footer';

class Layout extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Header/>
        {this.props.children}
      <div role="dialog" tabIndex={-1} id="modal-search" aria-labelledby="modal-search" aria-hidden="true" className="fade search-index-modal in modal">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                <span className="sr-only">Close</span>
              </button>
              <h4 className="modal-title"><span>Tôi muốn tìm player với điều kiện</span></h4>
            </div>
            <div className="content modal-body">
              <table className="table">
                <tbody>
                  <tr>
                    <td className="text-left">
                      <span>Giới tính là</span>
                    </td>
                    <td className="text-right">
                      <div data-toggle="buttons" className="btn-group">
                        <label type="button" className="btn btn-default">
                          <span><i className="fa fa-mars" aria-hidden="true" />&nbsp;<span>Nam</span></span>
                          <input type="radio" name="sex" defaultValue={0} />
                        </label>
                        <label type="button" className="btn btn-default">
                          <span><i className="fa fa-user-secret" aria-hidden="true" />&nbsp;<span>Nữ</span></span>
                          <input type="radio" name="sex" defaultValue={1} />
                        </label>
                      </div>
                    </td>
                    <td>
                      <div data-toggle="buttons" className="btn-group">
                        <label type="button" className="btn btn-default">
                          <span><i className="fa fa-times" aria-hidden="true" /></span>
                        </label>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td className="text-left">
                      <span>Hiện đang là</span>
                    </td>
                    <td className="text-right">
                      <div data-toggle="buttons" className="btn-group">
                        <label type="button" className="btn btn-default">
                          <span><i className="fa fa-user-circle" aria-hidden="true" />  new</span>
                          <input type="radio" name="is_currently" defaultValue={0} />
                        </label>
                        <label type="button" className="btn btn-default">
                          <span><i className="fa fa-user-secret" aria-hidden="true" />  hot</span>
                          <input type="radio" name="is_currently" defaultValue={1} />
                        </label>
                        <label type="button" className="btn btn-default">
                          <span><i className="fa fa-user-md" aria-hidden="true" />  vip</span>
                          <input type="radio" name="is_currently" defaultValue={2} />
                        </label>
                      </div>
                    </td>
                    <td>
                      <div data-toggle="buttons" className="btn-group">
                        <label type="button" className="btn btn-default">
                          <span><i className="fa fa-times" aria-hidden="true" /></span>
                        </label>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td className="text-left">
                      <span>Trạng thái là</span>
                    </td>
                    <td className="text-right">
                      <div data-toggle="buttons" className="btn-group">
                        <label type="button" className="btn btn-default">
                          <input name="status" type="radio" autoComplete="off" defaultValue="ready" />
                          <span><i className="fa fa-spinner" aria-hidden="true" />&nbsp;<span><span>Sẵn sàng</span></span></span>
                        </label>
                      </div>
                    </td>
                    <td>
                      <div data-toggle="buttons" className="btn-group">
                        <label type="button" className="btn btn-default">
                          <span><i className="fa fa-times" aria-hidden="true" /></span>
                        </label>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td className="text-left">Đang online</td>
                    <td className="text-right">
                      <div data-toggle="buttons" className="btn-group">
                        <label type="button" className="btn btn-default">
                          <span><i className="fas fa-globe-africa" />&nbsp; Online</span>
                          <input type="radio" name="online" defaultValue={1} />
                        </label>
                      </div>
                    </td>
                    <td>
                      <div data-toggle="buttons" className="btn-group">
                        <label type="button" className="btn btn-default">
                          <span><i className="fa fa-times" aria-hidden="true" /></span>
                        </label>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td className="text-left">
                      <span>Sống tại</span>
                    </td>
                    <td className="text-right">
                      <div className="mb-4" />
                      <div className="player-select div-autoComplete-search">
                        <input className="filter-modal" name="citySelect" type="text" defaultValue />
                        <div className="suggestion-game-hide" />
                      </div>
                    </td>
                    <td>
                      <div data-toggle="buttons" className="btn-group">
                        <label type="button" className="btn btn-default">
                          <span><i className="fa fa-times" aria-hidden="true" /></span>
                        </label>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td className="text-left">Tên/Url player:</td><td className="text-right">
                      <div className="player-select div-autoComplete-search">
                        <input className="filter-modal" name="nickName" type="text" autoComplete="off" defaultValue />
                      </div>
                    </td>
                    <td>
                      <div data-toggle="buttons" className="btn-group">
                        <label type="button" className="btn btn-default">
                          <span><i className="fa fa-times" aria-hidden="true" /></span>
                        </label>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td className="text-left">Tags:</td>
                    <td className="text-right">
                      <div className="player-select div-autoComplete-search">
                        <input className="filter-modal" name="tagsString" type="text" autoComplete="off" maxLength={60} defaultValue />
                      </div>
                    </td>
                    <td>
                      <div data-toggle="buttons" className="btn-group">
                        <label type="button" className="btn btn-default">
                          <span><i className="fa fa-times" aria-hidden="true" /></span>
                        </label>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="text-center modal-footer">
              <button className="btn btn-danger">Lọc</button>
            </div>
          </div>
        </div>
      </div>
      <Footer/>
    </div>
    );
  }
}

export default Layout;