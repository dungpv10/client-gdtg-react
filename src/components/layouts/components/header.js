import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {
  logo, 
  flag
} from '../../../assets';
import SingleChat from './header/chat/single-chat';
import GroupChat from './header/chat/group-chat';
import Notification from './header/chat/notification';
import UserMenu from './header/user-menu';
class Header extends Component {
  render() {
    return (
      <header id="header" className="header">
        
          <div className="container">
              <div className="row">
                  <div className="col-lg-5 col-md-4 col-sm-7 col-xs-3 left">
                      <div className="header-left">
                          <Link to="/" className="logo">
                              <img style={{maxWidth: '123px'}} src={logo} alt="logo" />
                          </Link>
                          <button className="nav-icon collapsed" data-toggle="collapse" href="#menu-left" role="button" aria-expanded="false" aria-controls="menu-left" type="button"><i className="fas fa-bars false"></i></button>
                          <form action="#" method="post" className="form-search-header" acceptCharset="utf-8">
                              <input type="text" name="search" placeholder="Nhập tên thành viên/giao dịch viên" />
                              <button type="submit" className="search"><i className="fa fa-search" aria-hidden="true"></i></button>
                          </form>
                          <div className="collapse menu-left" id="menu-left">
                              <ul className="menu-header-left">
                                  <li>
                                      <Link to='/'>Trang chủ</Link>
                                  </li>
                                  <li>
                                    <Link to='/social'>Cộng đồng</Link>
                                  </li>
                                  <li>
                                      <a href="#">Lĩnh vực</a>
                                  </li>
                                  <li>
                                    <Link to='/rank'>BXH</Link>
                                  </li>
                              </ul>
                          </div>
                      </div>
                  </div>
                  <div className="col-lg-7 col-md-8 col-sm-5 col-xs-9 right">
                      <div className="header-right clearfix">
                          <div className="country-flag">
                              <img src={flag} alt="VN" />
                          </div>
                          <ul className="menu-header-right">
                              <li>
                                <Link to='/'>Trang chủ</Link>
                              </li>
                              <li>
                                  <Link to='/social'>Cộng đồng</Link>
                              </li>
                              <li>
                                <Link to='/rank'>BXH</Link>
                              </li>
                          </ul>
                          
                          <div className="wrap-after-login">
                              <ul className="menu-after-login">
                                  <Notification/>
                                  <GroupChat/>
                                  <SingleChat/>
                              </ul>
                              <UserMenu/>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </header>
        
    );
  }
}

export default Header;