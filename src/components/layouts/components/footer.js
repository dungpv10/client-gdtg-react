import React, {Component} from 'react';
import {
  multiLanguage
} from '../../../assets';

class Footer extends Component {
  render() {
    return (
      <footer className="footer" id="footer" style={{marginTop: '10px'}}>
      <div className="footer-header">
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <a >
                <label>Support 24/7</label>
                <p>Admin <b>(1000 đ/phút)</b></p>
              </a>  
            </div> 
            <div className="col-md-4 hide-on-768">
              <a >
                <label>Support 24/7</label>
                <p>Giao dịch viên <b>(1000 đ/phút)</b></p>
              </a>  
            </div> 
            <div className="col-md-4 hide-on-768">
              <a >
                <label>Support 24/7</label>
                <p>Kỹ thuật <b>(1000 đ/phút)</b></p>
              </a>  
            </div> 
          </div>
        </div>
      </div>
      <div className="footer-middle">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="footer-services">
                <div className="row">
                  <div className="col-md-3 col-sm-6 col-xs-12">
                    <h4>Giaodich.me</h4>
                    <ul>
                      <li>
                        <a  rel="nofollow">Giới thiệu</a>
                      </li>
                      <li>
                        <a  rel="nofollow">Liên hệ</a>
                      </li>
                      <li>
                        <a  rel="nofollow">Tuyển dụng</a>
                      </li>
                      {/* <li>
                                          <a  rel="nofollow">Mắt Bão BPO</a>
                                      </li> */}
                    </ul>
                  </div>
                  <div className="col-md-3 col-sm-6 col-xs-12">
                    <h4>Thông tin cần biết</h4>
                    <ul>
                      <li>
                        <a  rel="nofollow">Giới thiệu</a>
                      </li>
                      <li>
                        <a  rel="nofollow">Liên hệ</a>
                      </li>
                      <li>
                        <a  rel="nofollow">Mua bán</a>
                      </li>
                      {/* <li>
                                          <a  rel="nofollow">Mắt Bão BPO</a>
                                      </li> */}
                    </ul>
                  </div> 
                  <div className="col-md-3 col-sm-6 col-xs-12">
                    <h4>Câu hỏi thường gặp</h4>
                    <ul>
                      <li>
                        <a  rel="nofollow">Giới thiệu</a>
                      </li>
                      <li>
                        <a  rel="nofollow">Liên hệ</a>
                      </li>
                      <li>
                        <a  rel="nofollow">Kho nick</a>
                      </li>
                      {/* <li>
                                          <a  rel="nofollow">Mắt Bão BPO</a>
                                      </li> */}
                    </ul>
                  </div> 
                  <div className="col-md-3 col-sm-6 col-xs-12">
                    <h4>Tin tức</h4>
                    <ul>
                      <li>
                        <a  rel="nofollow">Giới thiệu</a>
                      </li>
                      <li>
                        <a  rel="nofollow">Liên hệ</a>
                      </li>
                      <li>
                        <a  rel="nofollow">Kho nick</a>
                      </li>
                      <li>
                        <a  rel="nofollow">Mua bán</a>
                      </li>
                    </ul>
                  </div> 
                </div>
              </div>
              <div className="footer-social-box">
                <div className="row">
                  <div className="col-md-5 col-sm-12">
                    <h4>ĐĂNG KÝ NHẬN THÔNG TIN</h4>
                    <form action="#" method="get" acceptCharset="utf-8">
                      <input type="text" placeholder="Nhập email của bạn" className="txt-regemail" name="Email" />
                      <input type="button" defaultValue="Đăng ký" className="btn-regemail submitform btn-pm" />
                    </form>
                  </div>
                  {/* <div class="col-md-7 col-sm-12 right">
                                  <h4>&nbsp;</h4>
                                  <img src="images/dky_BCT.svg" alt="">
                                  <img src="images/dky_BCT.svg" alt="">
                                  <img src="images/dky_BCT.svg" alt="">
                              </div> */}
                </div>
              </div>
              <div className="conect-width-me clearfix">
                <h4>Kết nối giao dịch</h4>
                <div>
                  <a rel="nofollow"  className="btn-social btn-facebook">
                    <img src="https://www.matbao.net/Content/images/icon-fb-normal.png" />
                  </a>
                  <a rel="nofollow"  className="btn-social btn-gplus">
                    <img src="https://www.matbao.net/Content/images/icon-gg-normal.png" />
                  </a>
                  <a rel="nofollow"  className="btn-social btn-twitter">
                    <img src="https://www.matbao.net/Content/images/icon-tt-normal.png" />
                  </a>
                  <div className="language-wrap">
                    <img src={multiLanguage} width={22} alt="earth" />
                    <select className="languege" name="languege">
                      <option value="VN">Tiếng việt</option>
                      <option value="ENG">English</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="footer-location">
                <div className="row">
                  <div className="col-md-4 col-sm-12 element">
                    <p>TRỤ SỞ CHÍNH</p>
                    <span>Tầng 3 ngõ 43 số nhà 32 Hà Nội, Việt Nam</span>
                  </div>
                  <div className="col-md-4 col-sm-12 element">
                    <p>TRỤ SỞ CHÍNH</p>
                    <span>Tầng 3 ngõ 43 số nhà 32 Hà Nội, Việt Nam</span>
                  </div>
                  <div className="col-md-4 col-sm-12 element">
                    <p>TRỤ SỞ CHÍNH</p>
                    <span>Tầng 3 ngõ 43 số nhà 32 Hà Nội, Việt Nam</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="footer-bottom">
        <div className="container">
          <div className="row">
            <div className="col-md-8 col-sm-6 col-xs-12">
              <p>Copyright© Quanplay Company. All Reserved.</p>
              <p>Sử dụng nội dung ở trang này và dịch vụ tại Quanplay có nghĩa là bạn đồng ý với 
                <a target="_blank"  rel="nofollow">Thỏa thuận sử dụng</a> và 
                <a target="_blank"  rel="nofollow"> Chính sách bảo mật </a> của chúng tôi.
              </p>
              <p> Giấy phép kinh doanh số: 1234567 cấp ngày 04/09/2002 bởi Sở Kế Hoạch và Đầu Tư Tp. Hà Nội</p>
              <p>Giấy phép cung cấp dịch vụ Viễn thông số 247/GP-CVT cấp ngày 08 tháng 05 năm 2018 </p>
            </div>
            <div className="col-md-4 col-sm-6 col-xs-12">
              <h4>WEBSITE CÙNG QUANPLAY GROUP</h4>
              <div className="doi-tac">
                <a className="img-doi-tac" ><img alt="AXYS" src="images/logo-ODS.svg" /></a>
                <a className="img-doi-tac" ><img alt="AXYS" src="images/logo-S8.svg" /></a>
                <a className="img-doi-tac" ><img alt="AXYS" src="images/logo-ODS.svg" /></a>
                <a className="img-doi-tac" ><img alt="AXYS" src="images/logo-S8.svg" /></a>
                <a className="img-doi-tac" ><img alt="AXYS" src="images/logo-ODS.svg" /></a>
                <a className="img-doi-tac" ><img alt="AXYS" src="images/logo-S8.svg" /></a>
              </div>
            </div>
          </div>
        </div>    
      </div>
    </footer>
  
        
    );
  }
}

export default Footer;