import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import 'react-slidedown/lib/slidedown.css'

class UserMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: false,
    };
    this.handleToggle = this.handleToggle.bind(this);
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({
        toggle: false
      });
    }
  }

  handleToggle() {
    this.setState({
      toggle: !this.state.toggle
    });
  }

  render() {
    return (
        <div ref={this.setWrapperRef} className="info-user clearfix">
          <div onClick={this.handleToggle}>
            <img  alt="avatar" src="https://gateway.playerduo.com/upload-service/avatar18.png" />
            <div className="info">
                <p>your customer name</p>
                <p className="money">0&nbsp;đ</p>
            </div>
          </div>
          <div className={"collapse collapse-header detail-info-user " + (this.state.toggle ? 'in' : '')} id="detail-info-user">
              <div className="content">
              <div className="header-dropdown">
                  <div className="user-id-rs">your customer name</div>
                  <div className="user-id">ID: <span>dungpv</span>
                  </div>
              </div>
              <div className="main">
                  <div className="text-center">
                  <div className="avt avt-md">
                      <img alt="avatar" src="https://gateway.playerduo.com/upload-service/avatar18.png" />
                  </div>
                  <p className="money-rs">0 đ</p>
                  </div>
                  <div className="menu">
                  <p>
                      <a href="/"><i className="fa fa-plus" aria-hidden="true" />Nạp tiền</a>
                  </p>
                  <p>
                      <a href="/customer_bank/register"><i className="fas fa-minus-square" aria-hidden="true" />Rút tiền</a>
                  </p>
                  <p>
                      <a href="/"><i className="fas fa-credit-card" aria-hidden="true" />Mua Player Card</a>
                  </p>
                  <p>
                      <a href="/customer_setting/security"><i className="fas fa-plus" aria-hidden="true" />Tạo khóa bảo vệ</a>
                  </p>
                  <p>
                      <a href="/customer_history/duo_history"><i className="fa fa-clock" aria-hidden="true" />Lịch sử giao dịch</a>
                  </p>
                  <p>
                      <a href="/customer_follow"><i className="fa fa-users" aria-hidden="true" />Danh sách theo dõi</a>
                  </p>
                  <p>
                      <a href="/pay_info"><i className="fab fa-cc-amazon-pay" />Cài đặt ví điện tử</a>
                  </p>
                  <p>
                      <a href="/customer_setting/info"><i className="fa fa-cogs" aria-hidden="true" />Cài đặt tài khoản</a>
                  </p>
                  <p>
                      <a href="/player_static"><i className="fa fa-cogs" aria-hidden="true" />Cài đặt Player</a>
                  </p>
                  <p>
                      <a href="/"><i className="fa fa-power-off" aria-hidden="true" />Đăng xuất</a>
                  </p>
                  </div>
              </div>
              </div>
          </div>
        </div>
    );
  }
}

export default UserMenu;