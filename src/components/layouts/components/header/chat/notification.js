import React, {Component} from 'react';
import {SlideDown} from 'react-slidedown';
import 'react-slidedown/lib/slidedown.css'

class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: false,
    };
    this.handleToggle = this.handleToggle.bind(this);
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({
        toggle: false
      });
    }
  }

  handleToggle() {
    this.setState({
      toggle: !this.state.toggle
    });
  }

  render() {
    return (
    <li>
        <SlideDown className={'pure-menu pure-menu-scrollable dropdown-slidedown overlay'}>
          <a ref={this.setWrapperRef} title="" data-toggle="collapse" onClick={this.handleToggle} role="button" aria-expanded="false" aria-controls="notification-user">
              <i className="fa fa-bell" aria-hidden="true"></i>
          </a>
          <div ref={this.setWrapperRef} className={(!this.state.toggle ? 'collapse' : '') + " collapse-header comment-notification collapse-header-style-1"}>
            <div className="content">
                              <div>
                                  <div className="infinite-scroll-component " style={{overflow: 'hidden'}}>
                                    <ul>
                                      <li className="notification-item">
                                          <a href="/group/5d01e9a55a6fd27cd7540ca9">
                                              <div className="msg">
                                                  <div className="avt avt-md">
                                                      <img alt="avatar" src="https://gateway.playerduo.com/upload-service/upload_b1728bca9bbebded5549934c359050ff.jpg" />
                                                  </div>
                                                  <div className="detail">
                                                      <p><strong>Văn Đại</strong>&nbsp;cũng đã bình luận về một bài viết mà bạn quan tâm.</p>
                                                      <p className="time">
                                                          <span>5 phút trước</span>
                                                      </p>
                                                  </div>
                                              </div>
                                          </a>
                                      </li>
                                      </ul>
                                  </div>
                              </div>
                          </div>
          </div>
             
        </SlideDown>
                 
    </li>
    );
  }
}

export default Notification;