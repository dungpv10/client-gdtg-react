import React, {Component} from 'react';
import {SlideDown} from 'react-slidedown';
import {Link} from 'react-router-dom';
import 'react-slidedown/lib/slidedown.css'

class GroupChat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: false,
    };
    this.handleToggle = this.handleToggle.bind(this);
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({
        toggle: false
      });
    }
  }


  handleToggle() {
    this.setState({
      toggle: !this.state.toggle
    });
  }

  render() {
    return (
    <li>
        <SlideDown className={'pure-menu pure-menu-scrollable dropdown-slidedown overlay'}>
        <a ref={this.setWrapperRef} title="" data-toggle="collapse" onClick={this.handleToggle} role="button" aria-expanded="false" aria-controls="friend-user">
            <i className="fa fa-users" aria-hidden="true"></i>
        </a>
        <div ref={this.setWrapperRef} className={(!this.state.toggle ? 'collapse' : '') + " collapse-header comment-group collapse-header-style-1"}>
            <div className="content">
                <div>
                    <div className="infinite-scroll-component " style={{overflow: 'hidden'}}>
                        <Link to="/message/5d0210a6fef7cb31d6ea59de">
                          <ul>
                            <li className="notification-item">
                                <div className="msg">
                                    <div className="avt avt-md">
                                        <img alt="avatar" src="https://gateway.playerduo.com/upload-service/upload_ffbe241b6e348b70ec0b7d88f266ca52.jpg" />
                                    </div>
                                    <div className="detail">
                                        <p><strong>♥️ Hương Zuto</strong> </p>
                                        <p style={{color: 'rgb(51, 51, 51)', textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden', width: '350px'}}>
                                            <span><i className="fa fa-reply" aria-hidden="true" style={{fontSize: '9px'}}></i></span>Hello, coder test ! 
                                        </p>
                                        <p className="time"><span>2 phút trước</span></p>
                                    </div>
                                </div>
                            </li>
                          </ul>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
             
        </SlideDown>
                 
    </li>
    );
  }
}

export default GroupChat;