import React, {Component} from 'react';
import Layout from '../layouts';
class Message extends Component { 
  constructor(props) {
    super(props);
    this.startChat = this.startChat.bind(this);
  }

  componentDidMount() {
    window.RSChat.init({
      socketUrl: 'http://chat.animals.vn/',
      userId: '1',
      name: 'User1',
      avatar: 'http://chat.animals.vn/images/user1.png'
    });
  }

  startChat(event) {
    window.RSChat.start('2', 'User 2');
  }

  render() {
    return (<Layout>
        <section className="section mt-95">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div className="widget-slider">
                <ul>
                  <li className="notification-item" onClick={this.startChat}>
                      <div className="msg">
                          <div className="avt avt-md">
                              <img alt="avatar" src="https://gateway.playerduo.com/upload-service/upload_ffbe241b6e348b70ec0b7d88f266ca52.jpg" />
                          </div>
                          <div className="detail">
                              <p><strong>♥️ Hương Zuto</strong> </p>
                              <p style={{color: 'rgb(51, 51, 51)', textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden', width: '350px'}}>
                                  <span><i className="fa fa-reply" aria-hidden="true" style={{fontSize: '9px'}}></i></span>Hello, coder test ! 
                              </p>
                              <p className="time"><span>2 phút trước</span></p>
                          </div>
                      </div>
                  </li>
                </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
    </Layout>);
  }
}

export default Message;