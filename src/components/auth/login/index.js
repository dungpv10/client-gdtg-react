import React, {Component} from 'react';
import LoginLayout from "../layout";

import {connect} from 'react-redux';
import {Link, Redirect} from 'react-router-dom';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Button from 'react-validation/build/button';
import {required} from '../../../validators';
import {postAuthLogin} from "../../../actions";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            redirect: false
        };
        this.login = this.login.bind(this);
    }

    changeInput(inputName, e) {
        e.preventDefault();
        let currentTarget = e.currentTarget;
        this.state[inputName] = currentTarget.value;

        this.setState(this.state);
    }

    login(e) {
        e.preventDefault();
        this.form.validateAll();

        if (this.checkBtn.context._errors.length === 0) {
            this.props.dispatch(postAuthLogin({
                username: this.state.username,
                password: this.state.password
            }));
            this.setRedirect();
        }
    }

    setRedirect() {
        if (localStorage.getItem('token') !== null) {
            this.state.redirect = true;
            this.setState(this.state);
        }
    }

    renderRedirect() {
        if (this.state.redirect || localStorage.getItem('token') !== null) {
            
            return <Redirect to={{pathname: '/', state: {message : 'Login successfully'}}}/>
        }
    }

    componentDidMount() {
        this.renderRedirect();
    }

    render() {
        const {errorMessage} = this.props;
        return (<LoginLayout>
            {this.renderRedirect()}
            <div className="body">
                <Form onSubmit={this.login} ref={c => {
                    this.form = c
                }}>
                    <div className="msg">Đăng nhập</div>
										<div className="input-group text-danger">
												{errorMessage}
										</div>
                    <div className="input-group">
                        <span className="input-group-addon">
                            <i className="material-icons">person</i>
                        </span>
                        <div className="form-line">
                            <Input
                                onChange={(e) => this.changeInput('username', e)}
                                validations={[required]} type="text"
                                className="form-control" name="username"
                                label='Tên đăng nhập'
                                placeholder="Tên đăng nhập" autoFocus/>
                        </div>
                    </div>
                    <div className="input-group">
                        <span className="input-group-addon">
                            <i className="material-icons">lock</i>
                        </span>
                        <div className="form-line">
                            <Input
                                onChange={(e) => this.changeInput('password', e)}
                                validations={[required]}
                                label='Mật khẩu'
                                type="password" className="form-control" name="password"
                                placeholder="Mật khẩu"/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <button className="btn btn-block bg-pink waves-effect" type="submit">Đăng nhập
                            </button>
                            <Button ref={c => {
                                this.checkBtn = c
                            }} style={{display: 'none'}} type={'submit'}>Đăng nhập</Button>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <Link to='/register' style={{color: 'white'}}
                                  className="btn btn-block bg-primary waves-effect"> Đăng ký</Link>
                        </div>
                    </div>
                </Form>
            </div>
        </LoginLayout>);
    }
}

export default connect(function (state) {
    const {postAuthLogin} = state;

    return postAuthLogin;
})(Login);
