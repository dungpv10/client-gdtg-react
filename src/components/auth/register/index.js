import React, {Component} from 'react';
import LoginLayout from "../layout";
import {connect} from 'react-redux';
import {Link, Redirect} from 'react-router-dom';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Button from 'react-validation/build/button';
import {required, passwordConfirm} from '../../../validators';
import {postAuthRegister} from "../../../actions/auth";

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
						name: '',
            username: '',
            password: '',
						passwordConfirmation: '',
            redirect: false
        };
				this.register = this.register.bind(this);
    }

    changeInput(inputName, e) {
        e.preventDefault();
        const currentTarget = e.currentTarget;
        this.state[inputName] = currentTarget.value;
        this.setState(this.state);
    }

    register(e) {
        e.preventDefault();
        this.form.validateAll();

        if (this.checkBtn.context._errors.length === 0) {
            this.props.dispatch(postAuthRegister(this.state));
            this.setRedirect();
        }
    }

		setRedirect() {
        if (localStorage.getItem('token') !== null) {
            this.state.redirect = true;
            this.setState(this.state);
        }
    }

		componentDidMount() {
        this.renderRedirect();
    }

		renderRedirect() {
        if (this.state.redirect || localStorage.getItem('token') !== null) {
            return <Redirect to='/admin/users'/>
        }
    }

    render() {
        const {errorMessage} = this.props;
        return (<LoginLayout>
						{this.renderRedirect()}
            <div className="body">
                <Form onSubmit={this.register} ref={c => {
                    this.form = c
                }}>
                    <div className="msg">Đăng ký</div>
										<div className="input-group text-danger">
												{errorMessage}
										</div>

										<div className="input-group">
                        <span className="input-group-addon">
                            <i className="material-icons">person</i>
                        </span>
                        <div className="form-line">
                            <Input
                                onChange={(e) => this.changeInput('name', e)}
                                validations={[required]} type="text"
                                className="form-control" name="name"
                                label='Họ và tên'
                                placeholder="Họ và tên" autoFocus/>
                        </div>
                    </div>


                    <div className="input-group">
                        <span className="input-group-addon">
                            <i className="material-icons">person</i>
                        </span>
                        <div className="form-line">
                            <Input
                                onChange={(e) => this.changeInput('username', e)}
                                validations={[required]} type="text"
                                className="form-control" name="username"
                                label='Tên đăng nhập'
                                placeholder="Tên đăng nhập"/>
                        </div>
                    </div>
                    <div className="input-group">
                        <span className="input-group-addon">
                            <i className="material-icons">lock</i>
                        </span>
                        <div className="form-line">
                            <Input
                                onChange={(e) => this.changeInput('password', e)}
                                validations={[required]}
                                label='Mật khẩu'
                                type="password" className="form-control" name="password"
                                placeholder="Mật khẩu"/>
                        </div>
                    </div>
										<div className="input-group">
                        <span className="input-group-addon">
                            <i className="material-icons">lock</i>
                        </span>
                        <div className="form-line">
                            <Input
                                onChange={(e) => this.changeInput('passwordConfirmation', e)}
                                validations={[required, passwordConfirm]}
                                label='Xác nhận mật khẩu'
                                type="password" className="form-control" name="passwordConfirmation"
                                placeholder="Xác nhận mật khẩu"/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <button className="btn btn-block bg-pink waves-effect" type="submit">Đăng ký
                            </button>
                            <Button ref={c => {
                                this.checkBtn = c
                            }} style={{display: 'none'}} type={'submit'}>Đăng  ký</Button>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <Link to='/login' style={{color: 'white'}}
                                  className="btn btn-block bg-primary waves-effect"> Đăng nhập</Link>
                        </div>
                    </div>
                </Form>
            </div>
        </LoginLayout>);
    }
}

export default connect(function (state) {
    const {postAuthRegister} = state;
		return postAuthRegister;
})(Register);
