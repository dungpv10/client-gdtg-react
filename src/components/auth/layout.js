import React, {Component} from 'react';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
import '../../assets/auth';
// import {SOCIAL_KEY} from '../../consts/auth/login';

class LoginLayout extends Component {

    constructor(props) {
        super(props);
        this.handleClickFacebook = this.handleClickFacebook.bind(this);
        this.handleCallbackFacebook = this.handleCallbackFacebook.bind(this);
        this.handleResponseGoogle = this.handleResponseGoogle.bind(this);
        this.handleGoogleOnFailure = this.handleGoogleOnFailure.bind(this);
    }

    handleClickFacebook() {
       
        //Handle click login facebook
    }

    handleCallbackFacebook(response) {
        //handle login facebook
    }

    handleGoogleOnFailure(response) {
        console.log(response);
    }

    handleResponseGoogle(response) {
        const userInformation = response.profileObj;
    
        const data = {
            ...userInformation,
            name: [userInformation.givenName, userInformation.familyName].join(' ')
        };

        // this.props.dispatch(login(data));
    }


    render() {
        return (
            <div className='auth'>
            <div className=" login-page ">
                <div className="login-box">
                    <div className="logo">
                        <a href="javascript:void(0);"><b>GDTG</b></a>
                        <small>Trung gian giao dịch, chuyển nhượng</small>
                    </div>
                    <div className="card" style={{marginBottom: 0}}>
                        {this.props.children}
                        <div className="body">
                        
                        </div>
                    </div>
                    
                    
                </div>
                
            </div>
            </div>
        );
    }
}


export default LoginLayout;
