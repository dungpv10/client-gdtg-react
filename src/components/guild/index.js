import React, {Component} from 'react';
import Layout from '../layouts';

class Guild extends Component { 
  constructor(props) {
    super(props);
  }

  render() {
    return (<Layout>
        <section className="section mt-95">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div className="widget-slider">
                  Guild
                </div>
              </div>
            </div>
          </div>
        </section>
    </Layout>);
  }
}

export default Guild;