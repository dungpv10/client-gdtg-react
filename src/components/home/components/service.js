import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Modal from 'react-modal';
import {
  service
} from '../../../assets';
import CreateTrade from './service/create-trade';

class Service extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <section className="section had-gb-style-1">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div className="widget widget-service">
                  <div className="widget-content">
                    <div className="row">
                      <div className="col-md-3 col-sm-6 col-xs-12">
                        <div className="service-item">
                          <img src={service} />
                          <h4>Hướng dẫn</h4>
                          <p>Đảm bảo hiệu suất hoạt động 99,9% và cam kết bồi hoàn khi có sự cố.Đảm bảo hiệu suất hoạt động 99,9% và cam kết bồi hoàn khi có sự cố</p>
                          
                          <Link to="/guild" className="btn">Xem chi tiết</Link>
                        </div>
                      </div>
                      <CreateTrade/>
                      
                      <div className="col-md-3 col-sm-6 col-xs-12">
                        <div className="service-item">
                          <img src={service} />
                          <h4>Kho nick</h4>
                          <p>Đảm bảo hiệu suất hoạt động 99,9% và cam kết bồi hoàn khi có sự cố.Đảm bảo hiệu suất hoạt động 99,9% và cam kết bồi hoàn khi có sự cố</p>
                          
                          <a href="#" className="btn">Xem chi tiết</a>
                        </div>
                      </div>
                      <div className="col-md-3 col-sm-6 col-xs-12">
                        <div className="service-item">
                          <img src={service} />
                          <h4>Dịch vụ khác</h4>
                          <p>Đảm bảhi có sự cố.Đảm bảo hiệu suất hoạt động 99,9% và cam kết bồi hoàn khi có sự cố</p>
                          
                          
                          <a href="#" className="btn" >Xem chi tiết</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

    );
  }
}

export default Service;