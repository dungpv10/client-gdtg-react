import React, {Component} from 'react';
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {
  slide1, 
  slide2
} from '../../../assets';

class Slider extends Component {
  render() {
    return (
      <section className="section mt-95 section-bg-style-1">
            <div className="container">
              <div className="row">
                <div className="col-lg-3 col-md-3 col-sm-12 left">
                    <div className="widget widget-top-recharge-card">
                        <div className="widget-content">
                            <div className="title-content">
                                <span>Nạp thẻ</span>
                                <p>Top nạp thẻ tháng 06</p>
                            </div>
                            <ul className="list-style-2">
                                <li className="clearfix">
                                    <span className="position">1</span>
                                    <a href="#" title="">tên 1</a>
                                    <span className="number-recharge">3,000,000<span>đ</span></span>
                                </li>
                                <li className="clearfix">
                                    <span className="position">2</span>
                                    <a href="#" title="">tên 2</a>
                                    <span className="number-recharge">3,000,000<span>đ</span></span>
                                </li>
                                <li className="clearfix">
                                    <span className="position">3</span>
                                    <a href="#" title="">tên 3</a>
                                    <span className="number-recharge">3,000,000<span>đ</span></span>
                                </li>
                                <li className="clearfix">
                                    <span className="position">4</span>
                                    <a href="#" title="">tên 4</a>
                                    <span className="number-recharge">3,000,000<span>đ</span></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="col-lg-9 col-md-9 col-sm-12">
                  <div className="widget widget-slider">
                    <Carousel infiniteLoop={true} autoPlay showThumbs={false}>
                      <div className="item">
                        <img src={slide1} alt="Chicago" />
                      </div>
                      <div className="item">
                        <img src={slide2} alt="Chicago" />
                      </div>
                    </Carousel>
                  </div>
                </div>
              </div>
            </div>
          </section>
    );
  }
}

export default Slider;