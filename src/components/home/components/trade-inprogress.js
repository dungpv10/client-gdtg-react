import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {getProcessingTrades} from '../../../actions';
import {
  API_URL
} from '../../../consts';
class TradeInProgress extends Component {
  componentDidMount() {
    this.props.dispatch(getProcessingTrades());
  }
  render() {
    const {trades} = this.props.getProcessingTrades;
    console.log('trades', trades)
    return (
      <section className="section no-margin">
      <div className="container">
      <div className="row">
        <div className="col-md-12">
          <div className="widget widget-vip-player">
            <div className="widget-title clearfix">
              <h4 className="title">{this.props.title} ({trades.length})</h4>
              <button className="refresh-btn">
                <i className="fas fa-sync" aria-hidden="true" />
                <span> Làm mới</span>
              </button>
            </div>
            <div className="widget-content">
              <div className="row custom-row">
                {trades && trades.map((trade, index) => {
                  return (
                    <div key={index} className="col-md-3 col-sm-6 col-xs-12">
                    <div className="player-information-card-wrap">
                      <div className="player-avatar style1">
                        <img src={`${API_URL}${trade.gameId.imagePath}`} alt="player avatar" />
                        <img src={`${API_URL}${trade.gdvId.avatarPath}`} alt="" className="img-child" />
                        {/* <div className="player-text">Bảo hiểm</div>
                        <div className="player-price">{user.feeTrade | 0}</div> */}
                        {/* <div className="hidden-information">
                          <p><span>{trade.description}<br /></span></p>
                          <div className="rent"><Link to='/'> Thuê ngay</Link></div>
                        </div> */}
                      </div>
                      <div className="player-information style1">
                        <h3 className="player-name">
                          <Link to='/'>Game Company</Link>
                        </h3>
                        <div className="row">
                          <div className="col-md-12 col-xs-12">
                            <p className="player-title"><span>Game</span> : Liên quan mobile <br/> Liên quan mobile </p>
                          </div>
                        </div>
                        <div className="row had-custom-reposive">
                          <div className='wrap-title-status'>
                            <span className="player-title">ID: {trade.id}</span>
                            <div className={'player-status ' + (trade.loginStatus ? 'ready' : 'busy')}> 
                            </div>
                          </div>
                        
                          
                          {/* <p className='total-trade'>{user.totalTrade || 0} giao dịch thành công</p> */}
                        </div>
                      </div>
                    </div>    
                  </div>
                  );
                })}
                {trades && trades.length > 4 && (
                    <div className="col-md-12 text-center">
                    <button type="button" className="btn btn-success btn-load-more">
                        <span>Xem thêm</span>
                    </button>
                </div>  
                )}
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </section>
    );
  }
}

export default connect(function(state) {
  const {getProcessingTrades} = state;
  return {getProcessingTrades};
})(TradeInProgress);