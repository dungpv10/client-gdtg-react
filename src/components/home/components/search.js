import React, {Component} from 'react';

class Search extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <section className="section">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div className="widget widget-search">
                  <form action="#" method="post" className="form-search-widget" acceptCharset="utf-8">
                    <div className="text-right col-md-3">&nbsp;</div>
                    <input type="text" name="gameCategory" />
                    <button type="button" onClick={this.handleShow} className="btn btn-danger btn-filter" >
                      <span>Bộ lọc</span>
                    </button>
                    
                    <button type="button" className="btn btn-success btn-search">
                      <span>Tìm kiếm</span>
                    </button>
                    <button type="button" className="button-icon-search"><i className="fa fa-search" aria-hidden="true" /></button>
                    <button type="button" className="button-icon-filter" data-toggle="modal" data-target="#modal-search"><i className="fa fa-bars setting-search-index" aria-hidden="true" /></button>
                  </form>
                </div>
              </div>
            </div>
          </div>
          
        </section>
    );
  }
}

export default Search;