import React, {Component} from 'react';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Button from 'react-validation/build/button';
import {required} from '../../../../validators';
import {
    service
} from '../../../../assets';

class CreateTrade extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            price: 0,
            siteB: ''
        };
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    changeInput(inputName, e) {
      e.preventDefault();
      let currentTarget = e.currentTarget;
      this.state[inputName] = currentTarget.value;

      this.setState(this.state);
    }

    setWrapperRef(node) {
        this.wrapperRef = node;
    }

    handleClickOutside(event) {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.handleCloseModal();
        }
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleCloseModal() {
        this.setOpen(false);
    }

    handleOpenModal() {
        this.setOpen(true);
    }

    setOpen(value) {
        this.setState({
            open: value
        });
    }

    handleSubmit(e) {
      e.preventDefault();
      this.form.validateAll();
      if (this.checkBtn.context._errors.length === 0) {

      }
    }

    render() {
        return (<div className="col-md-3 col-sm-6 col-xs-12">
                <div className="service-item">
                    <img src={service}/>
                    <h4>Mua và bán</h4>
                    <p>Đảm bảo hiệu suất hoạt động 99,9% và cam kết bồi hoàn khi có sự cố.Đảm bảo hiệu suất hoạt động
                        99,9% và cam kết bồi hoàn khi có sự cố</p>

                    <button ref={this.setWrapperRef} onClick={this.handleOpenModal} className="btn">Tạo giao dịch
                    </button>
                    <div style={this.state.open ? {display: 'block'} : {display: 'none'}}
                         className="fade search-index-modal in modal">
                        <div className="modal-dialog">
                            <div className="modal-content" ref={this.setWrapperRef}>
                                <div className="modal-header">
                                    <button onClick={this.handleCloseModal} type="button" className="close"
                                            data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                        <span className="sr-only">Close</span>
                                    </button>
                                    <h4 className="modal-title"><span>Tạo giao dịch</span></h4>
                                </div>
                                <div className="content modal-body">
                                    <Form ref={c => {
                                        this.form = c
                                    }}>
                                        <table className="table">
                                            <tbody>
                                            <tr>
                                                <td className="text-left">
                                                    <span>Bên B:</span>
                                                </td>
                                                <td className="text-right">
                                                  <Input
                                                      onChange={(e) => this.changeInput('siteB', e)}
                                                      validations={[required]}
                                                      label='Bên B'
                                                      value={this.state.siteB}
                                                      type="text" className="form-control" name="siteB"
                                                      placeholder="Bên B"/>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td className="text-left">
                                                    <span>Tên game</span>
                                                </td>
                                                <td className="text-right">
                                                    <select className='form-control'>
                                                        <option>Chọn game</option>
                                                        <option>Name 1</option>
                                                    </select>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td className="text-left">
                                                    <span>Kiểu giao dịch</span>
                                                </td>
                                                <td className="text-right">
                                                    <select className='form-control'>
                                                        <option>Chọn kiểu giao dịch</option>
                                                        <option>Mua</option>
                                                        <option>Bán</option>
                                                        <option>Trao đổi</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td className="text-left">Giá tiền</td>
                                                <td className="text-right">
                                                <Input
                                                    onChange={(e) => this.changeInput('price', e)}
                                                    validations={[required]}
                                                    label='Giá tiền'
                                                    value={this.state.price}
                                                    type="number" className="form-control" name="price"
                                                    placeholder="Giá tiền"/>
                                                </td>

                                              </tr>
                                            </tbody>
                                        </table>
                                    </Form>
                                </div>
                                <div className="text-center modal-footer">
                                    <button className="btn btn-danger">Tạo giao dịch</button>
                                    {/* <Button ref={c => {
                                        this.checkBtn = c
                                    }} style={{display: 'none'}} type={'submit'}>Tạo giao dịch</Button> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CreateTrade;
