import React, {Component} from 'react';
import Layout from '../layouts';
import Slider from './components/slider';
import Service from './components/service';
import Search from './components/search';
import Gdv from './components/gdv';
import TradeInProgress from './components/trade-inprogress';

class Home extends Component { 
  constructor(props) {
    super(props);
  }

  render() {
    return (<Layout>
        <Slider />
        <Service />
        <Search />
        <Gdv title='Giao dịch viên ' />

        <TradeInProgress title='Giao dịch đang chờ xử lý' />
        {/* <Gdv title='Giao dịch viên được thuê nhiều nhất' /> */}
    </Layout>);
  }
}

export default Home;