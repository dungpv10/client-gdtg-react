import React, {Component} from 'react';
import Layout from '../layouts';
import {
  gdv, 
  life
} from '../../assets';
class Social extends Component { 
  constructor(props) {
    super(props);
  }

  render() {
    return (<Layout>
        <section className="section mt-95">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div className="widget-slider">
                  Social
                </div>
              </div>
            </div>
          </div>
        </section>
    </Layout>);
  }
}

export default Social;