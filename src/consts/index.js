import {
  GET_DGV, GET_PROCESSING_TRADE
} from './home';
import {LOGIN_CONST, REGISTER_CONST} from './auth';
export const API_URL = 'http://api.animals.vn/';
export {
  GET_DGV,
  LOGIN_CONST,
  REGISTER_CONST,
  GET_PROCESSING_TRADE
};