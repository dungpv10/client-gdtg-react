export const SOCIAL_KEY = {
    facebookAppId : '246754549324120',
    googleClientId: '782913887615-72pdat13hs2rm3do1ocfk0mpjpegc2lm.apps.googleusercontent.com'
};

const LOGIN_CONST = {
    LOGIN_CONST_REQUEST: 'LOGIN_CONST_REQUEST',
    LOGIN_CONST_SUCCESS: 'LOGIN_CONST_SUCCESS',
    LOGIN_CONST_FAIL: 'LOGIN_CONST_FAIL',
};

export default LOGIN_CONST;
